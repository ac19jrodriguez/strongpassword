/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package password;
import java.util.Random;
/**
 *
 * @author Jordi
 */
public class Password {
    
    int longitud;
    String password;

    public Password(String password) throws Exception {
        longitud = password.length();
        if (longitud >= 6 && longitud <=10) {
            this.password = password;
        }else{
            throw new Exception ("La contraseña debe de tener de 6 a 10 caracteres");
        }
    }

    public int getLongitud() {
        return longitud;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;   
    }
    
    
    public boolean isStrong(){
        String password = this.password;
        boolean num = false,lower = false, upper = false,simbolo = false, strong = false;
        
        if( password.matches("(?=.*[0-9]).*") ){
            num = true;
        }
        if( password.matches("(?=.*[a-z]).*") ){
            lower = true;
        }
        if( password.matches("(?=.*[A-Z]).*") ){
            upper = true;    
        }
        if( password.matches("(?=.*[~!@#$%^&*()_-]).*") ){
            simbolo = true;
        }
        if (num&&lower&&upper&&simbolo) {
            strong = true;
        }
        
        return strong;

    }
    
    public static String passwordAleatoria(){
        int longitud = 10;
        String simbolos = "-/.^&*_!@%=+>)"; 
        String mayus = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
        String minus = "abcdefghijklmnopqrstuvwxyz"; 
        String numeros = "0123456789";
        String finalString = mayus + minus + 
                numeros + simbolos; 
        Random random = new Random(); 
        String password = "";

        for (int i = 0; i < longitud; i++) 
        { 
            password += 
                    finalString.charAt(random.nextInt(finalString.length())); 

        } 
        
        return password;

    }
}
