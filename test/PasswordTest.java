/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import password.Password;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static password.Password.passwordAleatoria;

/**
 *
 * @author Jordi
 */
public class PasswordTest {
    
    public PasswordTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
        public void passwordLarga() throws Exception {
        Password p = new Password("Holaquetalcomoestas12345");
        assertFalse (p.isStrong());
    }
    @Test
        public void passwordCorta() throws Exception {
        Password p = new Password("hola");
        assertFalse  (p.isStrong());
    }
    @Test
        public void passwordStrong() throws Exception {
        Password p = new Password("Jordi1234!");
        assertTrue  (p.isStrong());
    }
    @Test
        public void passwordSinMayus() throws Exception {
        Password p = new Password("jordi1234!");
        assertFalse  (p.isStrong());
    }
    @Test
        public void passwordSinMinus() throws Exception {
        Password p = new Password("JORDI1234!");
        assertFalse  (p.isStrong());
    }
    @Test
        public void passwordSinSimbolo() throws Exception {
        Password p = new Password("JORDI1234");
        assertFalse  (p.isStrong());
    }
    @Test
        public void passwordSinNumero() throws Exception {
        Password p = new Password("JORDIjordi!");
        assertFalse  (p.isStrong());
    }
        
        
    // Si este test falla es porque la pass generada no es totalmente segura
    @Test
        public void randomPassword() throws Exception {
        Password p = new Password(passwordAleatoria());
        assertTrue  (p.isStrong());
    }
        
    
    
    
    
    
    
    
    
    
    
    
}
